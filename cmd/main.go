package main

import (
	"gitlab.com/green_tea/go_todo_project"
	"gitlab.com/green_tea/go_todo_project/package/handler"
	"gitlab.com/green_tea/go_todo_project/package/repository"
	"gitlab.com/green_tea/go_todo_project/package/service"
	"log"
)

func main() {
	repos := repository.NewRepository()
	services := service.NewService(repos)
	h := handler.NewHandler(services)

	server := new(todo.Server)

	err := server.Run("5555", h.InitRoutes())

	if err != nil {
		log.Fatalf("error occured while run server: %s", err.Error())
	}
}
