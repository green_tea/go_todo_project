package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/green_tea/go_todo_project/package/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler  {
	return &Handler{services: services}
}

func (handler *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		auth.POST("/sign-up", handler.signUp)
		auth.POST("/sign-in", handler.signIn)
	}

	api := router.Group("/api")
	{
		lists := api.Group("/lists")
		{
			lists.POST("/", handler.createList)
			lists.GET("/", handler.allLists)
			lists.GET("/:id", handler.getList)
			lists.PUT("/:id", handler.updateList)
			lists.DELETE("/:id", handler.deleteList)

			items := lists.Group("/:id/items")
			{
				items.POST("/", handler.createListItem)
				items.GET("/", handler.allListItems)
				items.GET("/:id", handler.getListItem)
				items.PUT("/:id", handler.updateListItem)
				items.DELETE("/:id", handler.deleteListItem)
			}
		}
	}

	return router
}